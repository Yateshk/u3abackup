﻿using System;
using System.IO;
using System.IO.Compression;
using System.Threading;
using System.Threading.Tasks;
using System.Text.Json;
using System.Collections;
using System.Reflection;
using System.Text.Json.Serialization;
//using System.Text.RegularExpressions;
using System.Linq;
using AngleSharp;
using System.Net.Http;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Core;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.CommandLine.IO;
//using Serilog.Sinks;
//using Serilog.Settings.Configuration;


namespace U3ABackup
{
    class Program
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
         public static readonly AppSettings conf = new AppSettings();
   
        //static Dictionary<string, object> appConfig = new Dictionary<string, object>();
        static  Uri uri;
         static Uri baseUri;
        static string settingsFile;
        



       // static readonly U3APage aPage = new U3APage();
         //static HttpClient client = new HttpClient();

        //static readonly  Analyse analyse = new Analyse();
       static readonly Util util = new Util();

        public static AngleSharp.Dom.IDocument  documentP { get; private set; }
       static  readonly string docsFilesDefault =Path.Combine( Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),"U3A_New_Backups");
        static string backupsTablesDir ;
        static string backupsDatabaseDir;
       static  string backupsDirectoriesDir;
        static string backupsBase;
        public static readonly  string passPhrase = "U3A_HolmesChapel_8972";
        static string u3awebPasswd;
       static string logFilePath;
        public static Serilog.Core.Logger logF;       
        public static StreamWriter fatalLog;
        public static bool debugSwitch;
        public static bool completedDirectories;
        public static bool completedDatabase;
        public static bool completedTables;
        public static bool detectedErrors;
        public static string defaultConfigFile = "U3ABackupSettings.json";
        static string settingsFileName;
        static readonly List<string> settingsElements = new List<string>{ "LogSettings","U3AWebSettings","FolderSettings","EmailSettings" };

        // private static string any;
        // static LoggerConfiguration logF;

        static async Task Main(params string[] args)

        {
            bool showVersion = false;




            //string securityText ="zz"; // Force not find security text
           detectedErrors = false;
            // string downLoadPath = "backup_download.php?";
            //  string directoriesPath = "data_directories.php";
            //Serilog.Core.Logger  

            // Console.WriteLine("Getting ready press a key");
            // Console.ReadKey();
            //logF = genLogFile();
            //    logF.Debug("Got Log File");
            // Crash out if the Initialise has failed
            {  settingsFileName = defaultConfigFile;
                var cmd = new RootCommand
                {
                    //new Argument<string>("name", "Your name."),
                    // new Option<string?>("--greeting", "The greeting to use."),
                  
           new Option<string>( new[]{ "--config","-c" },
                      getDefaultValue: () => defaultConfigFile,"alternative configuration json file"),
              new Option<bool>(
                      new[]{ "--debug","-d" },
                               "use debug logging level"),
                           new Option<bool>(
                      new[]{ "--version","-v" },
                               "display version number")

    };

                // THIS WORKED  cmd.Handler = CommandHandler.Create< string?, IConsole>(handleCommandLine);
                // BUT this might be neater
                cmd.Handler = CommandHandler.Create<string,bool,bool>((config,debug,version) =>
                {
                    if (version)
                    {
                        showVersion = true;
                        var vers = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
                        Console.WriteLine($"U3ABackup Version = {vers}");
                        return;

                    }
                    if (!String.IsNullOrEmpty(config))
                    {
                        Console.WriteLine($"The value for --config is: {config}");
                        settingsFileName = config;
                    }
                    if (debug)
                    {
                     debugSwitch = true;
                        // override the configuration file Minimum Level to be Debug
                    }
                    Console.WriteLine($"The value for --debug is: {debug}");

                   

                });

              var a=   cmd.Invoke(args);
            }
            if (showVersion) return; // exit if only showing version
            if (!Initialise(settingsFileName)) return;
            logF.Debug("Initialised"); 
         
            logF.Information($"Files will be saved in {backupsBase}");
            
      
            //****  setup
            
            //*****
            var context = BrowsingContext.New(Configuration.Default);

            //******  
            //***********get initial page ***********
            //*************
            var content = await U3APage.openPage(Program.uri);
            var document = await context.OpenAsync(req => req.Content(content));
            var fields = Analyse.getLoginFields(document, conf.loginFormIDs);

            logF.Debug("now logging in");
           // Console.WriteLine("now logging in logF =");
            //Console.WriteLine(logF.);
           //   Console.ReadKey();
            var userIDName = fields["userId"];
            var userPwdName = fields["userPwd"];
           
           
            //var body = "contactEmail=99999" + "&password=" + password;
            var body = $"{ userIDName}=99999&{ userPwdName}={u3awebPasswd}";
            var formContent = new FormUrlEncodedContent(new[] // from https://stackoverflow.com/questions/20005355/how-to-post-data-using-httpclient
             {
                new KeyValuePair<string, string>("contactEmail", "99999"),
                new KeyValuePair<string, string>("password", u3awebPasswd)
                    });
            //**************   
            //******************  LOGIN   *******
            //***********
            var contentP = await U3APage.postPage( uri, formContent);
             documentP = await context.OpenAsync(req => req.Content(contentP)); // get parsed logged in page
            //******************************************
            // search for the string 'Security Session' and ensure there's only One to check I'm logged in

            // if not found check that there is a logout within a link
            //**********************************************
            bool found = false;
           int pos = 0;
            int count = 0;
            string logoutText = "logout";

            // var zz0 =  Regex.Matches(contentP, "Security Sezzion").Cast<Match>().Select(m => m.Index);
            //  var zz = Regex.Matches(contentP, securityText).Cast<Match>().Select(m => m.Index)  ;  uses Linq
            //  if (!zz) { found = true};
            // while ((pos = contentP.IndexOf(securityText, pos, ignoreCase ? StringComparison.OrdinalIgnoreCase : StringComparison.Ordinal)) != -1)

           // securityText = conf.securityText;
            // loop for each element in the array of text indicators of being logged in to the security session
            foreach (string aText in conf.securityText)
            {
                while ((pos = contentP.IndexOf(aText, pos, StringComparison.CurrentCultureIgnoreCase)) != -1)
                {
                    pos++; count++; found = true;
                }
                if (found) break;
            }
            
            if (count > 1) { throw new Exception("Too many incidents of 'Security Session' text"); // this might indicate a problem
            }
            //  couldn't find the relevant text so look for a logout link'
            
            else if(!found) {
               
                // look for ?logout texts in an anchor

                foreach ( AngleSharp.Html.Dom.IHtmlAnchorElement aLink in documentP.Links)
            {
                 AngleSharp.Html.Dom.IHtmlAnchorElement  a = aLink;
                var b = a.Href;
                    if (aLink.Href.IndexOf(logoutText, 0, StringComparison.CurrentCultureIgnoreCase) != -1) {
                        found = true; break; }
            }
            }
            if (!found)
            {
                logF.Fatal($"Cannot find {conf.securityText} text nor {logoutText} links");
               return ;
            }
            //throw new Exception($"Cannot find {securityText} text nor logout links");
            //***********  We are logged in
            logF.Information("logged in");
            //   Console.ReadKey();
            //*********************************
            //***   get and download the tables *************
           // var completedTables = false;
            if (conf.gettables)
            {
                logF.Debug("Getting Tables");
               detectedErrors =  !await getTables();
                 completedTables = true;
            }

            if (conf.getdirectories)
            {
                //******************************
                //*****   Get Directories zip
                logF.Debug("Getting Directories");
                 var res =  getDirectories();
                while (!completedDirectories)
                {   Console.Write(".");
                    Thread.Sleep(500);
                }
              
            }
            if (conf.getdatabase)
            {
                logF.Debug("Getting Database");
                //**********************
                // Now get and save the database dump
                //*******************************
                //  detectedErrors = !await getDatabase(); 
                var resD = getDatabase();
                while (!completedDatabase)
                { Console.Write(".");
                    Thread.Sleep(500);
                }
               
            }
            if (detectedErrors)
            {
                logF.Information("******  Completed with Errors ******");
                if (conf.sendEmailOnFail)
                {
                    Util.sendFailureEmail(logFilePath);
                }
            }
            else
            {
                logF.Information(" Completed Successfully");
                logF.Information("******************************************************************************************\n");
                logF.Information("");
                if (conf.sendEmailOnSucceed)
                {
                    Util.sendSuccessEmail(logFilePath);
                }
            }

            if (debugSwitch)
            {
                logF.Information("Press any key to finish");
                 Console.ReadKey();
            }
                }
        /**
         *  check that all the settings values required are present in the settings file that's used
         *  This uses AppSettings.cs as the definition and checks that the json settings file contains at least those entries.
         *  Any other entries are ignored when the configuration is setup
         *  If an element is found to be missing crash out immediately
         *  It is assumed that at least the log file hasn't been created so report to Console and the Fatal log file U3ABackups_FATAL_ERROR.log
        
         */
        protected static bool checkSettings(string theFileName)
        {
            if (!File.Exists(theFileName))
            {
                Console.WriteLine($" File {settingsFile}, Does NOT Exist");
                fatalLog.WriteLine($" File {settingsFile}, Does NOT Exist");
                return false;

            }
           string  jsonString = File.ReadAllText(theFileName);
            JsonDocumentOptions options = new JsonDocumentOptions
            {  CommentHandling = JsonCommentHandling.Skip,
             AllowTrailingCommas = true
            };
            JsonDocument jsonObject;
            try { jsonObject = JsonDocument.Parse(jsonString, options); }
            catch (Exception e)
            {
                Console.WriteLine($" Problem with {settingsFile}, cannot Parse error {e}");
                fatalLog.WriteLine($" Problem with {settingsFile}, cannot Parse error {e}");
                return false;
            }
                // var authors = jsonObject.RootElement.EnumerateObject();
                // var authors = jsonObject.RootElement;
                // Get all the 1st level Elements from the json file into a List
                // then check that all the required sub-elements (settingsElements) exist
                var rootSettingsEnum = jsonObject.RootElement.EnumerateObject();
            // var one = jsonObject.RootElement.EnumerateArray();
            List<string> topElementsList = new List<string>();
            foreach (var anEl in rootSettingsEnum)
            {
                topElementsList.Add(anEl.Name.ToString());
               
            }
            IEnumerable<string> diff;
            try {  diff = settingsElements.Except(topElementsList); }
            catch (Exception e)
            {   Console.WriteLine($" Problem with {settingsFile}, cannot find top Level Elements error {e}");
                fatalLog.WriteLine($" Problem with {settingsFile}, cannot find top Level Elements error {e}");
                return false;
            }
            if (diff.Any())
            {    
                Console.WriteLine($" Problem with {settingsFile}, cannot find top Level Element {diff.First()}");
                fatalLog.WriteLine($" Problem with {settingsFile}, cannot find top Level Element {diff.First()} ");
                return false;
            }
            ArrayList arList = new ArrayList();
            foreach (var anEl in settingsElements)
            {
                JsonElement.ObjectEnumerator anEnum = jsonObject.RootElement.GetProperty(anEl).EnumerateObject();
                foreach (var aSubEl in anEnum)
                {
                    arList.Add(aSubEl.Name);
                }
            }

     /*       if (!jsonObject.RootElement.TryGetProperty("LogSettings", out JsonElement logSettingsEl))
            {
                //if ( logSettingsEl.ValueKind.ToString() == "Undefined")

                Console.WriteLine($" Inconsistencies in {settingsFile}, cannot find element for LogSettings");
                fatalLog.WriteLine($" Inconsistencies in {settingsFile}, cannot find element for LogSettings");
                return false;

            };

            var logSettingsEnum = logSettingsEl.EnumerateObject();

            if (!jsonObject.RootElement.TryGetProperty("BackupSettings", out JsonElement backupSettingsEl))
            {
                Console.WriteLine($" Inconsistencies in {settingsFile}, cannot find element for BackupSettings");
                fatalLog.WriteLine($" Inconsistencies in {settingsFile}, cannot find element for BackupSettings");
                return false;
            }
            var backupSettingsEnum = backupSettingsEl.EnumerateObject();
           // ArrayList arList = new ArrayList(); 

           foreach(var anEl in logSettingsEnum)
            {
               arList.Add(anEl.Name);
            }
            foreach (var anEl in backupSettingsEnum)
            {
                arList.Add(anEl.Name);
            }*/

            /*  JsonSerializerOptions options2 = new JsonSerializerOptions
              {
                  ReadCommentHandling = JsonCommentHandling.Skip
              };
            //  var jsonObject2 = JsonSerializer.Deserialize<Dictionary<string, string>>(jsonString,options2);*/
            //JsonElement bz2;
            // Now get the list of entries in AppSettings.cs
            // and check that each one is in the json file
            bool correct = true;
            foreach (var appS in typeof(AppSettings).GetProperties())
            {
                var aSetting = appS.Name;
                if (!arList.Contains(aSetting))
                {
                    correct = false;
                    Console.WriteLine($" Inconsistencies in {settingsFile}, cannot find element for {aSetting}"); 
                    fatalLog.WriteLine($" Inconsistencies in {settingsFile}, cannot find element for {aSetting}");
                    return false;
                }
            }
            return correct;
        }
        protected static bool Initialise(string settings)
        { // setup a FATAL LOG in case I can't create the proper log file
            fatalLog = File.CreateText(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                "U3ABackups_FATAL_ERROR.log"));
            // ********************
            //**** Check if the configuration FileName (settings) is Fully Rooted and Fully Qualified
            // if it is then use that filename as is rather than add it to LocalApplicationData
            if (Path.IsPathRooted(settings) && Path.IsPathFullyQualified(settings))
            {
                settingsFile = Path.GetFullPath(settings);
            }
            else
            {

                string location = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Programs\\U3ABackup");
                // handle the case of in the Visual Studio Debugger
                if (System.Diagnostics.Debugger.IsAttached)
                {
                    settingsFile = settings;//"U3ABackupSettings.json";
                }
                else
                {
                    // string location = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
                    // string location = Environment.GetFolderPath(Environment.SpecialFolder.Programs);
                    settingsFile = Path.Combine(location, settings);
                }
            }
            //***********check that the settings File is valid, complete and is likely to be sufficient for the log file creation ********
            //******************************************************************************************

            if (!checkSettings(settingsFile))
            {
                Console.WriteLine($" ERROR with {settingsFile} ABORTING");
                fatalLog.WriteLine($" EROR with {settingsFile} ABORTING");
               fatalLog.Flush();
                fatalLog.Close();
                return false;
            }

            var builder = new ConfigurationBuilder();
            builder.AddJsonFile(settingsFile, optional: false, reloadOnChange: false)
            //.AddCommandLine( )   
            ;
            IConfigurationRoot configRoot;
            // Bomb out if the config file can't be built
            try
            { configRoot = builder.Build();
            }
            catch (Exception e)
            {
                Console.WriteLine($"Failed adding the Json file to the config with error { e} using json file {settingsFile }");
                fatalLog.WriteLine($"Failed adding the Json file to the config with error { e} using json file {settingsFile }");
                fatalLog.Flush();
                fatalLog.Close();
                return false;
            }
            foreach(var aSetting in settingsElements)
            {
                ConfigurationBinder.Bind(configRoot.GetSection(aSetting), conf);
            }
       //     ConfigurationBinder.Bind(configRoot.GetSection("U3AWebSettings"), conf); // for handling web pages
      //      ConfigurationBinder.Bind(configRoot.GetSection("LogSettings"), conf);// for log file settings
      //      ConfigurationBinder.Bind(configRoot.GetSection("FolderSettings"), conf);// for Folder settings
      //      ConfigurationBinder.Bind(configRoot.GetSection("EmailSettings"), conf);// for Email settings


            var levelSwitch = new LoggingLevelSwitch();
          var  theLevel= Enum.Parse<Serilog.Events.LogEventLevel>(conf.MinimumLevel);
            if (debugSwitch) {levelSwitch.MinimumLevel = Enum.Parse<Serilog.Events.LogEventLevel>("Debug");
                 theLevel = Enum.Parse<Serilog.Events.LogEventLevel>("Debug");
            }
             // levelSwitch.MinimumLevel = theLevel;
                       
            if (!String.IsNullOrEmpty(conf.backupsBase)) { backupsBase = conf.backupsBase; }
            else
            {
                backupsBase = docsFilesDefault;
            }
            logFilePath = Path.Combine(backupsBase, conf.logFilePath); // log file path
            logF = new LoggerConfiguration()
               .MinimumLevel.ControlledBy(levelSwitch)
                .WriteTo.Console(outputTemplate: conf.consoleOutputTemplate)
                 .WriteTo.File(path: logFilePath,  outputTemplate: conf.fileOutputTemplate, restrictedToMinimumLevel:theLevel,
                  rollingInterval:conf.RollingInterval,retainedFileCountLimit:conf.retainedFileCountLimit)
                .CreateLogger();
            DateTime today = DateTime.Today;
            string todayStr = today.ToString("yyyyMMdd");
            string newLogFilePath = conf.logFilePath.Replace("_.txt", "_" + todayStr + ".txt");
            logFilePath = Path.Combine(backupsBase, newLogFilePath); // log file path
         
            logF.Information("\n\n******************************************************************************************");
            logF.Information($"Settings File used - {Path.GetFullPath(settingsFile)}");


            var readtext = new StreamReader(settingsFile);
            while (!readtext.EndOfStream)
            {
                logF.Debug(readtext.ReadLine());
            }

            var liveOrTest = conf.liveOrTest;
            logF.Information($"liveOrTest = { conf.liveOrTest}");
            logF.Debug($"testWebsiteBase = { conf.testWebsiteBase}");
            baseUri = new Uri(conf.testWebsiteBase);
            string websitepPath = conf.testWebsitePath;
            if ("Live" == liveOrTest)
            {
                baseUri = new Uri(conf.liveWebsiteBase);
                websitepPath = conf.liveWebsitePath;
            }
            
             uri = new Uri(baseUri, websitepPath);
            logF.Information("accessing site " + uri.ToString());

            //    Console.ReadKey();
            backupsTablesDir = conf.backupsTablesDir;
            backupsDirectoriesDir = conf.backupsDirectoriesDir;
            backupsDatabaseDir = conf.backupsDatabaseDir;

            
            
           // if ("" != conf.u3awebPasswdEncrypted)
                if (!String.IsNullOrEmpty(conf.u3awebPasswdEncrypted))
                { u3awebPasswd = StringCipher.Decrypt(conf.u3awebPasswdEncrypted, Program.passPhrase);}
            else
                {
                logF.Fatal($"No u3awebPasswdEncrypted supplied in {settings}");

                return false;
            }
           
            if(String.IsNullOrEmpty(conf.smtpPasswdEncrypted)&& (conf.sendEmailOnFail ||conf.sendEmailOnSucceed))
                  {
                logF.Fatal($"No smtpPasswdEncrypted value in {settings} yet emails are required");

                return false; }



                return true;
            
            



        }
        protected static Serilog.Core.Logger UNUSED_genLogFile()
        {  // *************  LOGGING Settings *************
       /***************  replace config method with explicit log creation  */
/*     var addConf = System.Diagnostics.Debugger.IsAttached ? "DEBUG" : "Production";
            string location = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Programs\\U3ABackup");
            if (System.Diagnostics.Debugger.IsAttached)
            {
                settingsFile = "U3ABackupSettings.json";
            }
            else
            {
                // string location = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
                // string location = Environment.GetFolderPath(Environment.SpecialFolder.Programs);
                settingsFile = Path.Combine(location, "U3ABackupSettings.json");
            }
            

            // logF.Information($" addCon = {addConf}");
            // logF.Information($" addCon = {addConf}");

            var builderLog = new ConfigurationBuilder()
               //.SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile(settingsFile, optional: false)
               .Build();
            var logF = new LoggerConfiguration()
                .ReadFrom.Configuration(builderLog)
                 .CreateLogger();
*/

            // logF.Debug("in logger");
            //logF.Information($" Settings file is {settingsFile}");
            return logF;
        }
            protected async static Task<Boolean> getTables()
        {
            //*********************************************
            //  Now find the links for table downloads and load the table names and url links
            //************************************************************
            logF.Debug("Getting Table Links");
            Dictionary<string, string[]> allTables = Analyse.getAllTables(documentP, conf.tablesText);

            //*********************************************
            //  Now download the tables and zip them up
            //************************************************************
            logF.Debug("Creating Table Directory");
            try
            { Directory.CreateDirectory(Path.Combine(backupsBase, backupsTablesDir)); }
            catch (Exception e)
            {
                logF.Error("Creating Tables directory process failed: {0}", e.ToString());
                completedTables = true;
                return false;
            }
            completedTables = true;
            if (await  util.saveTables(baseUri, allTables, Path.Combine(backupsBase, backupsTablesDir)))
            {
                return true;
            }
            else return false; ;
        }
        protected static async Task<bool> getDirectories()
        {
            //**********************
            // Now get and save the Directories zip 
            //*******************************
            try
            { Directory.CreateDirectory(Path.Combine(backupsBase, backupsDirectoriesDir)); }
            catch (Exception e)
            {
                logF.Error("Creating Direectories directory process failed: {0}", e.ToString());
                return false;
            }

            string directoriesLink = Analyse.getDirectoriesUrl(documentP, conf.directoriesText).TrimStart('/');
            // remove any leading '\' characters

            var a = await Util.saveDirectories(new Uri(baseUri, directoriesLink), Path.Combine(backupsBase, backupsDirectoriesDir));


            return a;
        }
        protected static  async Task<bool>   getDatabase()
        {
            try
            { Directory.CreateDirectory(Path.Combine(backupsBase, backupsDatabaseDir)); }
            catch (Exception e)
            {
                logF.Error("Creating Database directory process failed: {0}", e.ToString());
            }

            string databaseLink = Analyse.getDatabaseUrl(documentP, conf.databaseText).TrimStart('/');
           var a = await Util.saveDatabase(new Uri(baseUri, databaseLink), Path.Combine(backupsBase, backupsDatabaseDir));

            return a ;
        }
        /*static void handleCommandLine(string? config, IConsole console)
        {
            console.Out.WriteLine($" configuration file is {config}");
           // settingsFileName = config;
            return;
        }*/

    }
}
 