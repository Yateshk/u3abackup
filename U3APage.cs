﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AngleSharp;
using AngleSharp.XPath;
using System.Net.Http;

namespace U3ABackup
{
    class U3APage
    {
        public string userAgent;
        public static HttpClient client=new HttpClient();
        public Configuration config;
        public BrowsingContext context;
        public U3APage()
        {
            userAgent = "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36";
            
            client.DefaultRequestHeaders.Add("User-Agent", this.userAgent);
            config = new Configuration();
            context = new BrowsingContext(config);

        }
        public static async Task<string> openPage(Uri  Uri)
        {
            // var client = new HttpClient();
            // this.client.DefaultRequestHeaders.Add("User-Agent", this.userAgent);
            var content = await client.GetStringAsync(Uri);

            return content;

        }
        public static async Task< Tuple<bool,byte[]>> getBytes(string Uri)
        {
            byte[] content =  Array.Empty<byte>(); ;
            // var client = new HttpClient();
            // this.client.DefaultRequestHeaders.Add("User-Agent", this.userAgent);
            try
            {
                content = (byte[])await client.GetByteArrayAsync(Uri);
            }
            catch (Exception e)
            {
               
                Program.logF.Error($"GetBytes Failed with message {e.Message}");
                return new Tuple<bool, byte[]>(false, content);
            }

             return  new Tuple<bool,byte[]> (true,content);

        }
        public static async Task<string> postPage(Uri  Uri, FormUrlEncodedContent body)
        {
            // var client = new HttpClient();

            //client.DefaultRequestHeaders.Add("User-Agent", this.userAgent);
            var response = await client.PostAsync(Uri, body); // from https://stackoverflow.com/questions/47944400/how-do-i-use-httpclient-postasync-parameters-properly
            var content = await response.Content.ReadAsStringAsync();
            return content;
        }






    }
}
