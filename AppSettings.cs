﻿using System;
using System.IO;


namespace U3ABackup
{
    public class AppSettings
    {//public string testKey { get; set; }
        public string[] securityText { get; set; }
        public string MinimumLevel { get; set; }
        public string logFilePath { get; set; } 
        public Serilog.RollingInterval RollingInterval { get; set; }
        public int retainedFileCountLimit { get; set; }
        public string fileOutputTemplate { get; set; }
        public string consoleOutputTemplate { get; set; }
        public bool gettables { get; set; }
        public bool getdirectories { get; set; }
        public bool getdatabase { get; set; }
        public string liveWebsiteBase { get; set; }
        public string liveWebsitePath { get; set; }
        public string testWebsiteBase { get; set; }
        public string testWebsitePath { get; set; }
        public string liveOrTest { get; set; }
       public string tablesText { get; set; }

        public string[] directoriesText { get; set; }
        public string[] databaseText { get; set; }
        public string backupsTablesDir { get; set; }
        public string backupsDatabaseDir { get; set; }
        public string backupsDirectoriesDir { get; set; }

         public string backupsBase { get; set; }

        public bool sendEmailOnFail { get; set; }
        public bool sendEmailOnSucceed { get; set; }
        public string smtpServer { get; set; }
        public string smtpUsername { get; set; }
        public string smtpPasswdEncrypted { get; set; }
       // public string smtpPasswdPlain { get; set; }
        public string[] emailTo { get; set; }
        public string emailFrom { get; set; }
        
        public string u3awebPasswdEncrypted { get; set; }
        //public string u3awebPasswdPlain { get; set; }
        public string[] loginFormIDs { get; set; }
        




    }
}
