﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;
using static U3ABackup.Program;
using MailKit.Net.Smtp;
using MimeKit;
using MailKit.Security;

//using System.Diagnostics;

namespace U3ABackup
{
    class Util
    { protected string tmpDir;
       

        //protected class analyse; 
        public Util()
        {
            tmpDir = GetTemporaryDirectory();
           // var analyse = new Analyse();
        }
            
          static  protected string GetTemporaryDirectory()
        {
            string tempDirectory = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            Directory.CreateDirectory(tempDirectory);
            return tempDirectory;
        }
        public async Task<bool> saveTables( Uri baseUri, Dictionary<string, string[]> allTables, string docsFilesPath)
        {   string timeStamp = DateTime.Now.ToString("yyyyMMdd-HHmmss");
            //var stopwatch = Stopwatch.StartNew();
            foreach (KeyValuePair<string, string[]> aTable in allTables)
            {
                var dlUri = baseUri + aTable.Value[1];
             

               // dlUri = "http://holmeschapelu3a.org.uk/test_systemz/backup_download.php?id=1"; // LEY FORCE FAIL TEST
               logF.Debug($"Downloading Table from {dlUri}");
               // Console.WriteLine($"Downloading Table from {dlUri}");
                Tuple<bool, byte[]> contentB = await U3APage.getBytes(dlUri);
                if (contentB.Item1) // Item 1 is response true or false
                {
                  logF.Information($" Got csv {aTable.Value[0]} from U3AWeb  ");
               //     Console.WriteLine($" Got csv {aTable.Value[0]} from U3AWeb  ");


                    string fn = $"\\{aTable.Key}_{aTable.Value[0]}_{timeStamp}.csv";
                    //   string fileName = tmpDir + @"\"+ aTable.Value[0]+"-"+aTable.Key + timeStamp + ".csv";
                    string fileName = tmpDir + fn;
                 File.WriteAllBytes(@fileName, contentB.Item2);
                }
                else
                {
                    logF.Error($"tables download Failed on table {aTable.Value[0]}  " );
                    return false;

                }
            }

            //stopwatch.Stop();

          
            //Console.WriteLine($"Elapsed time:          {stopwatch.Elapsed}\n");

            // logF.Information("file written to " + fileName);
            // ****  zip them up
            ZipFile.CreateFromDirectory(tmpDir, docsFilesPath + @"\Backup_Tables_" + timeStamp + ".zip"); ;
            logF.Information("tables zip written to " + docsFilesPath + @"\Backup_Tables_" + timeStamp + ".zip");



            // Now empty the tmp files

            DirectoryInfo di = new DirectoryInfo(tmpDir);
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }
            //   File.Delete(fileName);
            Directory.Delete(tmpDir);
            return true;
        }
        public  static async Task<bool> saveDirectories(Uri directoriesUri, string docsFilesPath)
        {
            completedDirectories = false; // used in main to enable outputting dots until finished
           string thisUri = directoriesUri.ToString();
            logF.Information($"Getting Directories zip from U3AWeb  ");
         //  

            //var stopwatch = Stopwatch.StartNew();

            //  thisUri = "http://holmeschapelu3a.org.uk/test_system/data_directoriesz.php"; // FORCE FAIL TEST
            logF.Debug($"Downloading Directories zip from {thisUri}");
           
           // Console.WriteLine($"[{timeStamp} ] Downloading Directories zip from {thisUri}");
            Tuple<bool, byte[]> resp = await U3APage.getBytes(thisUri);
            //  Tuple<bool, byte[]> resp = await U3APage.getBytes(directoriesUri.ToString());

            //stopwatch.Stop();
           // Console.WriteLine($"Elapsed time:          {stopwatch.Elapsed}\n");

            if (resp.Item1)
            {
                
               string  timeStamp = DateTime.Now.ToString("yyyyMMdd-HHmmss");
                Console.WriteLine(); // tidy up after the dots showing progress
                logF.Information($"Got Directories zip from U3AWeb  ");

                //    string fn = $"\\{aTable.Key}_{aTable.Value[0]}_{timeStamp}.csv";
                //   string fileName = tmpDir + @"\"+ aTable.Value[0]+"-"+aTable.Key + timeStamp + ".csv";
                string fileName = docsFilesPath + @"\directories" + timeStamp + ".zip";
                File.WriteAllBytes(@fileName, resp.Item2);

                //       ZipFile.CreateFromDirectory(tmpDir, docsFilesPath + @"\Backup_Tables_" + timeStamp + ".zip"); ;
                logF.Information("Directories zip written to " + fileName);
                completedDirectories = true;
                return true;
            }
            else
            {   Console.WriteLine(); // tidy up after the dots showing progress
                logF.Error($"Directories download Failed   ");
                detectedErrors = true;  
                completedDirectories = true;
                return false;
                 }



        }
        public  static async Task<bool> saveDatabase(Uri databaseUri, string docsFilesPath)
        {
            string thisUri = databaseUri.ToString();
            logF.Information($"Getting Database zip from U3AWeb  ");
            string timeStamp = DateTime.Now.ToString("yyyyMMdd-HHmmss");

         //   var stopwatch = Stopwatch.StartNew();

            // thisUri = "http://holmeschapelu3a.org.uk/test_system/backup_downloadz.php"; // FORCE FAIL TEST
            logF.Debug($"Downloading Database zip from {thisUri}");
            Tuple<bool, byte[]> resp = await U3APage.getBytes(thisUri);

            //stopwatch.Stop();
        //    Console.WriteLine($"Elapsed time:          {stopwatch.Elapsed}\n");

            // byte[] contentB = U3APage.client.GetByteArrayAsync(databaseUri.ToString());
            if (resp.Item1)
            {
                Console.WriteLine(); // tidy up after the dots showing progress
                logF.Information($"Got Database zip from U3AWeb  ");

                //    string fn = $"\\{aTable.Key}_{aTable.Value[0]}_{timeStamp}.csv";
                //   string fileName = tmpDir + @"\"+ aTable.Value[0]+"-"+aTable.Key + timeStamp + ".csv";
                string fileName = docsFilesPath + @"\database" + timeStamp + ".zip";
                File.WriteAllBytes(@fileName, resp.Item2);

                //       ZipFile.CreateFromDirectory(tmpDir, docsFilesPath + @"\Backup_Tables_" + timeStamp + ".zip"); ;
                logF.Information("Database zip written to " + fileName);
                completedDatabase = true;
                return true;
            }
            else
            {
                Console.WriteLine(); // tidy up after the dots showing progress
                logF.Error("***Error Database Dump Failed ***");
                detectedErrors = true;
                completedDatabase = true;
                return false;
            }




        }
        public static bool  sendFailureEmail(string logFileName) {
            string timeStamp = DateTime.Now.ToString("yyyyMMdd-HHmmss");
            sendEmail(conf.emailFrom, conf.emailTo, $"U3ABackup Failure at {timeStamp}", $"U3A Backup Failed at {timeStamp} Log File at {logFileName}");
            return true;
        }
        public static bool sendSuccessEmail(string logFileName)
        {
            string timeStamp = DateTime.Now.ToString("yyyyMMdd-HHmmss");
            sendEmail(conf.emailFrom, conf.emailTo, $"U3ABackup succeeded at {timeStamp}", $"U3A Backup run Succeeded at {timeStamp} Log File at {logFileName}");
            return true;
        }

        protected  static bool sendEmail( string from, string[] to, string subject, string messageContent)
        { 
            //tb_messages.Text = " Email Sent";
            string smtpUserName ;
            string smtpPassword; string smtpServer;
            
            if(String.IsNullOrEmpty(conf.smtpServer)){  smtpServer = "mail.btinternet.com."; } else {  smtpServer = conf.smtpServer; }
            if (String.IsNullOrEmpty(conf.smtpUsername)) { smtpUserName = "laurie.l.yates@btinternet.com"; } else { smtpUserName = conf.smtpUsername; }
            if (String.IsNullOrEmpty(conf.smtpPasswdEncrypted)) {
                logF.Information(" *******   Error no smtp password supplied *******");
                return false;
            };
          
         //   if ("" != conf.smtpPasswdEncrypted) 
         //   if(!String.IsNullOrEmpty(conf.smtpPasswdEncrypted)){ 
                smtpPassword = StringCipher.Decrypt(conf.smtpPasswdEncrypted, passPhrase); 
            //}

          //  else smtpPassword = conf.smtpPasswdPlain;

            var  message = new MimeMessage();
            message.From.Add( MailboxAddress.Parse( from));
            message.To.Add(new MailboxAddress("", to[0]));
            message.Subject = subject;
           

            message.Body = new TextPart("plain")
            {
                Text = @messageContent
            };

            using (var client = new SmtpClient())
            {

              //  client.Connect(smtpServer, 587, true);
                client.Connect(smtpServer, 587, SecureSocketOptions.StartTls);

                // Note: only needed if the SMTP server requires authentication

                client.Authenticate(smtpUserName, smtpPassword);
  

                client.Send(message);
                client.Disconnect(true);
               
            }
            return true;
        }
    }
}
