﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AngleSharp;
using System.Text.RegularExpressions;

namespace U3ABackup
{
    class Analyse
    {

        /**
     * Looks through the parsed page and finds the form containing the login fields
     * identifies the field names for the login 
     * assumes the first field is the member number and the second the password
     * If it can't find them then returns an empty list
     */
        public static  Dictionary<string, string> getLoginFields(AngleSharp.Dom.IDocument document, string[] possLoginIDs)
        {
          
            var forms = document.Forms;

            // var loginForm = document.GetElementById()
            foreach (var aForm in forms)
            {  // look for forms withID's in Program.loginFormIDs
                foreach (var aLoginID in possLoginIDs)
                {
                    if ("post" == aForm.Method && aLoginID == aForm.Id)
                    {
                        // found the form I want}
                        return identifyLoginFieldNames(aForm);

                    }
                }
            }
            foreach (var aForm in forms)
            { 
                //couldn't find one of the expected form ids
                // Sooo choose first form that has a post and contains the word password?
                if ("post" == aForm.Method && aForm.InnerHtml.Contains("password", System.StringComparison.CurrentCultureIgnoreCase))
                {
                    //    var a = aForm.InnerHtml;
                    //    var inputs =  aForm.GetElementsByTagName("input");
                    // form contains the word password assume it's the one I want
                    return identifyLoginFieldNames(aForm);


                }

            }
            throw new Exception("Cannot find a Login Form");


        }
       protected static  Dictionary<string, string> identifyLoginFieldNames(AngleSharp.Html.Dom.IHtmlFormElement aForm)

        {
            Dictionary<string, string> resp = new Dictionary<string, string>();

          //  var a = aForm.InnerHtml;
            var inputs = aForm.GetElementsByTagName("input");
            // form contains the word password assume it's the one I want
            var userId = inputs[0].Id;
            resp.Add("userId", userId);
            var userPwd = inputs[1].Id;
            resp.Add("userPwd", userPwd);
            return resp;
        }
        /**
         * send it the (parsed) html content and get back a list of the tables to download
         * each list entry is keyed by the id number and contains the name of the table  
         */
      public static  Dictionary<string, string[]> getAllTables(AngleSharp.Dom.IDocument documentP, string searchTerm)
        {
            
            Dictionary<string, string[]> allTables = new Dictionary<string, string[]>();
            foreach (AngleSharp.Html.Dom.IHtmlAnchorElement aLink in documentP.Links)
            {//"backup_download.php?"
                if (aLink.Href.IndexOf(searchTerm, 0) != -1)

                { string tableNum = aLink.Search.Split("=")[1];
                    string tableName0 = aLink.Text;
                    Regex rgx = new Regex("[^a-zA-Z0-9]");
                   string tableName = rgx.Replace(tableName0, "");

                    string[] it = new string[] { tableName, aLink.PathName+aLink.Search};
                    allTables.Add(tableNum,it);
                    
                   

                }

            }

            return allTables;
        }
        public static  string getDatabaseUrl(AngleSharp.Dom.IDocument documentP,string[] param) => getTheUrl(documentP,param);
        public static string getDirectoriesUrl(AngleSharp.Dom.IDocument documentP, string[] param) => getTheUrl(documentP, param);
        public static string getTheUrl(AngleSharp.Dom.IDocument documentP, string[] searchTerm)
        {
          //  string resp = "";
           // bool found = false;
            foreach (string aSearchTerm in searchTerm) {
                foreach (AngleSharp.Html.Dom.IHtmlAnchorElement aLink in documentP.Links)
                { if (aLink.Text.IndexOf(aSearchTerm, 0) != -1)
                    {
                        //  var a = aLink;
                        //  var b = aLink.PathName;
                        return aLink.PathName;
                    }

                }
            }
                return "";
        }
/*        public string getDatabaseUrl0(AngleSharp.Dom.IDocument documentP, string[] searchTerm)
        {
            string resp = "";
            bool found = false;

            foreach (AngleSharp.Html.Dom.IHtmlAnchorElement aLink in documentP.Links)
            {
                if (aLink.Text.IndexOf(searchTerm, 0) != -1)
                {
                    //  var a = aLink;
                    //  var b = aLink.PathName;
                    return aLink.PathName;
                }

            }
            return "";
        }*/
    }
}

